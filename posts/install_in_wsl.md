---
title: Installing Neovim in WSL
date: Last Modified
---

![Computer screen with Linux and programming code, credit Lukas](https://unsplash.com/photos/NLSXFjl_nhc/download?force=true&w=640)

To get that Linux feeling with Neovim on Windows we are using the WSL.
Be ready for some crazy installation and ohhh the best feeling after completion.

## Prepare your Windows environment

- For installation I am using a Powershell in Windows Terminal.
  It has nice support for recognizing WSL images and later a nice user experience
  together with the FiraMono Nerdfont which worked best together with tmux and neovim.
  (Other extended Nerdfonts like Roboto, Ubuntu or Inconsolata
  showed wrong spacing or characters)
- You can install theses two via [scoop](https://scoop.sh/),
  which is a nice and easy to use package manager for software development
  ressources in Windows.

```shell
scoop add bucket extras
scoop install windows-terminal FiraMono-NF
```

- Now you can set the FiraMono Nerdfont as default in Windows Terminal.
  For that open the settings of Windows Terminal and open the settings JSON
  at the bottom left corner.
  Here you can set the newly installed font as default.

```json
...
"profiles":
    {
        "defaults":
            {
                "font": {"face": "FiraMono NF"},
            },
    ...
```

## Install Linux on Windows

- Given that you have Windows 11 WSL2 is already the standard.
  You can install WSL2 via `wsl --install` in a Powershell session in Windows Terminal.
  This will install the latest Ubuntu LTS.
- Sadly the Ubuntu LTS has rather old packages.
  Especially Nodejs is so old that we can't use it for the Neovim plugin coc.
  So we will uninstall Ubuntu with `wsl --unregister Ubuntu`.
- Instead of Ubuntu we use Alpine on WSL.
  Alpine has a nice image in the Microsoft Store that we will use.
  Install Alpine WSL and afterwards click open.
  This will open a terminal which will download the latest Alpine version
  and start it up.
- You should now also have a new entry in Windows Terminal
  for an Alpine Linux session.

## Configure Alpine

- Enter your username.
  The password you should set afterwards is currently the password for root
  and not the user.
- Now become root with `su` and with the above password to install packages
  and add a password for the user.

  ```shell
  apk add sudo curl neovim git ripgrep openssh nodejs npm python3 tmux
  ```

- Then `passwd {your username}` and enter password for your user.
- Ensure you are on the latest stable release.
  For that you have to edit the repositories.
  Run `setup-apkrepos` and choose the last option `e` to edit the repositories
  with a text editor.
  There you now replace the current version like `v3.15` with `latest-stable`
  and save with `:wq` if you are using vi as your text editor.
  Afterwards run `apk update` followed by apk upgrade --available.
- After the update you should do `sync` and then restart the image with `reboot`.
- Now you can check the new version with `cat /etc/alpine-release`.
- Currently the time zone is not automatically copied from Windows so you have to
  configure it yourself. You can easily do it with the following command based on
  the [Alpine Linux Documentation](https://wiki.alpinelinux.org/wiki/Alpine_setup_scripts#setup-timezone)

```shell
setup-timezone -z Europe/Berlin
```

- You can also further configure your Alpine Linux further for example
  by adding sudo etc.
  For more details have a look here. [Post installation recommendations for Alpine](https://docs.alpinelinux.org/user-handbook/0.1a/Working/post-install.html)
- Now switch back to your user with `su {your username}`.

## Setting up tmux

Add tmux gruvbox theme from [here](https://github.com/egel/tmux-gruvbox)
and insert the contents of `tmux-gruvbox-dark.conf` into `~/.tmux.conf`.

## Setting up Neovim

- Some keyboard shortcuts have to be deleted in the Windows Terminal preferences
  like ctrl-c or ctrl-v as they are needed in Neovim.
- Download the dotfiles from GitLab.
- Download and install Vim-Plug: `sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'`

- Copy the nvim and coc folders into `~/.config/`
- Move to the folder `~/.config/coc/extensions/`.
  In this folder we already copied the package.json for Nodejs.
  In order to install these packages we run `npm install`.
  This command will take the package.json and install its contents.
- Run `nvim` to open Neovim.
- In Neovim run `:PlugInstall`
- Now close Neovim with `:q!` and restart it.
- Everything should be installed now.

## Uninstall and/or reinstall Alpine

- You can uninstall Alpine with `wsl --unregister Alpine`.
- To reinstall simply click again on open at the Alpine WSL page
  in the Microsoft Store.

## Open Issues
- Update post to new lua config
- Luafy and improve readability of config
- Spell checker coc-ltex not working in Windows and WSL.
