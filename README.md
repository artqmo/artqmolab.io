![Status Build Pipeline](https://gitlab.com/artqmo/artqmo.gitlab.io/badges/main/pipeline.svg)

# Blog

A small blog as training for programming and editing skills with Neovim.
Forked from [Arpit Batra](https://github.com/arpitbatra123/eleventy-blog-mnml).

## Getting Started

### 1. Clone this Repository

```shell
git@gitlab.com:artqmo/artqmo.gitlab.io.git
```

### 2. Navigate to the directory

```shell
cd your-blog-name
```

### 3. Remove and add your remote origin

Copy everything from the folder `artqmo.gitlab.io` except the `.git` directory
into your own directory `your-blog-name`.

### 4. Install dependencies

```shell
npm install
```

### 5. Edit \_data/metadata.json

### 6. Run Eleventy

```shell
npm run serve
```

To build

```shell
npm run build
```

To clean the old files in /public

```shell
npm run clean
```
